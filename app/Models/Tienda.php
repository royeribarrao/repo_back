<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tienda extends Model
{
    protected $table = 'tiendas';
    protected $fillable = [
        'id',
        'business_name',
        'description',
        'state',
        'ruc',
        'logo',
        'departamento',
        'provincia',
        'distrito',
        'address',
        'codigo_postal',
        'email',
        'phone',
        'latitud',
        'longitud',
        'contacto'
    ];
}