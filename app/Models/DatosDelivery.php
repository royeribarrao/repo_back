<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatosDelivery extends Model
{
    protected $table = 'datos_delivery';
    protected $fillable = [
        'id',
        'nombres',
        'apellidos',
        'departamento',
        'provincia',
        'distrito',
        'direccion',
        'celular',
        'correo',
        'fecha_recojo',
        'latitud',
        'longitud'
    ];
}