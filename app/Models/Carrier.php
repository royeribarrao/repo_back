<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrier extends Model
{
    protected $table = 'carriers';
    protected $fillable = [
        'id',
        'codigo',
        'nombre',
        'ruc',
        'direccion',
        'telefono',
        'email',
        'responsable',
        'logo'
    ];

    public function tarifas() {
        return $this->hasMany(CarrierTarifa::class, 'carrier_id', 'id');
    }
}