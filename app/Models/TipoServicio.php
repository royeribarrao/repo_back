<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoServicio extends Model
{
    protected $table = 'tipo_servicio';
    protected $fillable = [
        'id',
        'codigo',
        'nombre',
        'nombre_tarifa_base',
        'precio_base'
    ];

    public function precios() {
        return $this->hasMany(PrecioTipoServicio::class, 'tipo_servicio_id', 'id');
    }
}