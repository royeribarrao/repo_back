<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoGestion extends Model
{
    protected $table = 'productos_gestion';
    protected $fillable = [
        'id',
        'sku_producto',
        'gestion_id'
    ];

    public function producto() {
        return $this->hasOne(Product::class, 'sku_producto', 'sku_producto');
    }

    public function gestion() {
        return $this->belongTo(Gestion::class, 'id', 'gestion_id');
    }
}