<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    protected $table = 'sales';
    protected $fillable = [
        'id',
        'tienda_id',
        'numero_compra',
        'fecha_compra',
        'sku_producto',
        'sku_descripcion',
        'marca',
        'cantidad',
        'sku_precio',
        'descuento',
        'precio_final',
        'nombre_cliente',
        'telefono',
        'email',
        'direccion',
        'distrito',
        'provincia',
        'departamento',
        'tipo_documento',
        'numero_documento'
    ];

    public function producto() {
        return $this->hasMany(Product::class, 'sku_producto', 'sku_producto');
    }
}