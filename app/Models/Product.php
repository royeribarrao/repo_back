<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'id',
        'sku_producto',
        'sku_description',
        'marca',
        'talla',
        'imagen',
        'stock',
        'sku_precio',
        'descuento',
        'precio_final',
        'dimensiones',
        'categoria'
    ];
}