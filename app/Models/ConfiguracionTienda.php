<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfiguracionTienda extends Model
{
    protected $table = 'configuracion_tienda';
    protected $fillable = [
        'id',
        'tienda_id',
        'devolucion',
        'estandar',
        'deluxe'
    ];
}