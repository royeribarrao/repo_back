<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NuevoProductoServicioCambio extends Model
{
    protected $table = 'nuevos_productos_servicio_cambio';
    protected $fillable = [
        'id',
        'sku_producto',
        'gestion_id'
    ];

    public function producto() {
        return $this->hasOne(Product::class, 'sku_producto', 'sku_producto');
    }

    public function gestion() {
        return $this->belongTo(Gestion::class, 'id', 'gestion_id');
    }
}