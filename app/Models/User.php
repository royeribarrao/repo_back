<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use App\Traits\Fullname;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable, HasFactory, Fullname;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'fullname',
        'father_lastname',
        'mother_lastname',
        'dni',
        'phone',
        'address',
        'distrito',
        'departamento',
        'state',
        'rol_id',
        'place_id',
        'password',
        'updated_user',
        'created_user',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    public function setPasswordAttribute($value){
        if ($value != ''){
            $this->attributes['password']= app('hash')->make($value);
        }
    }

    public function profile() {
        return $this->hasOne(Profile::class, 'id', 'rol_id');
    }

    public function sede() {
        return $this->hasOne(Sede::class, 'id', 'place_id');
    }
}
