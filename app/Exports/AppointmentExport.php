<?php

namespace App\Exports;

use App\Models\Appointment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class AppointmentExport implements FromCollection, WithStyles, WithColumnWidths
{

    protected $patientId;
    protected $typeExport;
    protected $addStudies;

    public function __construct($patientId, $typeExport, $addStudies)
    {
        $this->patientId = $patientId;
        $this->typeExport = $typeExport;
        $this->addStudies = $addStudies;
    }


    public function collection()
    {
        setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
        $typeExport = $this->typeExport;
        $addStudies = $this->addStudies ;

        if ($typeExport == 2 ) {
          return collect($this->getTreatments());
        }
        return collect($this->getControls());

    }

    public function getTreatments() {
      $arrCitas = [
          ['title' => 'CITA 1', 'code' => 'T01'],
          ['title' => 'CITA 2', 'code' => 'T02'],
          ['title' => 'CITA 3', 'code' => 'T03'],
          ['title' => 'CITA 4', 'code' => 'T04'],
          ['title' => 'CITA 5', 'code' => 'T05'],
      ];

      $patientInfo = \DB::table('patients as p')
        ->select(
          \DB::raw('UCASE(p.fullname) as nombre_paciente'),
        )
        ->where('id', $this->patientId)
        ->first();
        

      $apps = \DB::table('appointments as app')->select(
          'ac.name as code',
          'app.start_time as start_time'
        )
        ->leftJoin('appointments as app2', function($join)
          {
            $join->on('app.code', '=', 'app2.code');
            $join->on('app.start_time','<', 'app2.start_time');
          })
        ->leftJoin('appointment_codes as ac', 'ac.id', '=', 'app.code')
        ->whereNull('app2.start_time')
        ->where('app.patient_id', $this->patientId)
        ->where('ac.type', $this->typeExport)
        ->orderBy('ac.id', 'ASC')
        ->get();

      $orderedCells = [];
      $orderedCells[0] = ['Nombre', $patientInfo->nombre_paciente];
      $orderedCells[1][] = '';

      foreach($arrCitas as $key => $cita) {
        $startDate = 'DIA POR DEFINIR';
        $startTime = 'HORARIO POR DEFINIR';
        $itemFound = $this->searchForCode($cita['code'], $apps);
        if ($itemFound) {
          $startDate = date("d F", strtotime($itemFound->start_time));
          $startTime = date("h: i A", strtotime($itemFound->start_time));
        }

        $orderedCells[2][] = $cita['title'];
        $orderedCells[3][] = $startDate;
        $orderedCells[4][] = $startTime;
      }

      if ($this->addStudies == 1) {
        $orderedCells = $this->addStudies($orderedCells, 5);
      }
      return $orderedCells;
    }
    
    public function getControls() {
      $arrControl = [
        ['title' => 'PRIMER CONTROL', 'code' => 'CM1'],
        ['title' => 'SEGUNDO CONTROL', 'code' => 'CM2'],
        ['title' => 'TERCER CONTROL', 'code' => 'CM3'],
        ['title' => 'CUARTO CONTROL', 'code' => 'CM4'],
        ['title' => 'QUINTO CONTROL', 'code' => 'CM5'],
        ['title' => 'SEXTO CONTROL', 'code' => 'CM6'],
      ];
      $lastControl = 'CONTROL FINAL';
      $apps = [];

      $patientInfo = \DB::table('patients as p')
        ->select(
          \DB::raw('UCASE(p.fullname) as nombre_paciente'),
          'p.monthly_apps'
        )
        ->where('id', $this->patientId)
        ->first();

      $monthlyApps = $patientInfo->monthly_apps;
      $orderedCells = [
        ['Nombre', $patientInfo->nombre_paciente],
        ['']
      ];


      if ($monthlyApps > 0) {
        $apps = \DB::table('appointments as app')->select(
          'ac.name as code',
          'app.start_time as start_time',
        )
        ->leftJoin('appointments as app2', function($join)
          {
              $join->on('app.code', '=', 'app2.code');
              $join->on('app.start_time','<', 'app2.start_time');
          })
        ->leftJoin('appointment_codes as ac', 'ac.id', '=', 'app.code')
        ->whereNull('app2.start_time')
        ->where('app.patient_id', $this->patientId)
        ->where('ac.type', $this->typeExport)
        ->orderBy('ac.id', 'ASC')
        ->get();
      }

      if ($this->addStudies == 1) {
        $orderedCells = $this->addStudies($orderedCells, $monthlyApps);
      }

      return $orderedCells;
    }

    private function addStudies ($orderedCells, $index) {
      $orderedCells[2][$index] = 'ESTUDIOS RADIOLOGICOS';
      $orderedCells[3][$index] = 'DIA POR DEFINIR';
      $orderedCells[4][$index] = 'HORARIO POR DEFINIR';

      return $orderedCells;
    }
   
    private function searchForCode($code, $array) {
      foreach ($array as $key => $item) {
          if ($item->code === $code) {
              return $item;
          }
      }
      return null;
   }


    public function styles(Worksheet $sheet)
    {
        
        return [
            3  => ['font' => ['bold' => true, 'size' => 14], 'color' => '#008686'],
        ];
    }

    public function columnWidths(): array
    {
        return [
          'A' => 26,    
          'B' => 26,  
          'C' => 26,     
          'D' => 26,     
          'E' => 26,     
          'F' => 26,     
        ];
    }

}