<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Console\Commands\ActualizarTrackerDevolucion;
use App\Console\Commands\ActualizarTrackerCambioDeluxe;
use App\Console\Commands\ActualizarTrackerCambioEstandar;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ActualizarTrackerDevolucion::class,
        ActualizarTrackerCambioDeluxe::class,
        ActualizarTrackerCambioEstandar::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('actualizar_tracker_devolucion')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('actualizar_tracker_cambio_deluxe')
                  ->timezone('America/Lima')
                  ->everyMinute();
        $schedule->command('actualizar_tracker_cambio_estandar')
                  ->timezone('America/Lima')
                  ->everyMinute();
    }
}
