<?php

namespace App\Console\Commands;
use App\Models\TrackerDevolucion;
use App\Models\Gestion;
use App\Models\ServicioLogistico;

use Illuminate\Console\Command;

class ActualizarTrackerDevolucion extends Command
{

    protected $signature = 'actualizar_tracker_devolucion';

    protected $description = 'Actualiza todas las devoluciones, consumiendo el estatus de logix.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=';
        $gestiones = Gestion::where('en_proceso', true)
                                ->where('tipo_servicio', 3)
                                ->get();
        
        foreach($gestiones as $key => $gestion){
            $tracker = TrackerDevolucion::where('gestion_id', $gestion->id)->first();
            $gestion_actual = Gestion::find($gestion->id);
            $servicio_logistico = ServicioLogistico::where('gestion_id', $gestion->id)->first();
            $request = $client->request('GET', $url.$servicio_logistico->waybillNumber);
            $response = $request->getBody()->getContents();
            $str=str_replace("\r\n","",$response);
            $array_response = json_decode($str, true);
            $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];

            foreach($estados as $key => $item){
                if(isset($item['remarks'])){
                    // if($item['remarks'] == "Order has been assigned to an Olva driver" && $tracker->estado == 1){
                    //     $tracker->update([
                    //         'operador_logistico_confirmado' => true,
                    //         'nombre_estado' => 'Operador Logístico Confirmado',
                    //         'estado' => 2
                    //     ]);
                    //     $gestion_actual->update([
                    //         'estado' => 2
                    //     ]);
                    // }
                    if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2){
                        $tracker->update([
                            'en_camino' => true,
                            'nombre_estado' => 'En camino',
                            'estado' => 3
                        ]);
                        $gestion_actual->update([
                            'estado' => 3
                        ]);
                    }
                    if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3){
                        $tracker->update([
                            'producto_recogido' => true,
                            'nombre_estado' => 'Producto recogido',
                            'estado' => 4
                        ]);
                        $gestion_actual->update([
                            'estado' => 4
                        ]);
                    }
                    if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 4){
                        $tracker->update([
                            'producto_devuelto' => true,
                            'nombre_estado' => 'Producto devuelto',
                            'estado' => 5
                        ]);
                        $gestion_actual->update([
                            'estado' => 5
                        ]);
                    }
                }
            }
        }
    }
}