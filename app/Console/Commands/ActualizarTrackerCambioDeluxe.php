<?php

namespace App\Console\Commands;
use App\Models\TrackerCambioDeluxe;
use App\Models\Gestion;
use App\Models\ServicioLogistico;

use Illuminate\Console\Command;

class ActualizarTrackerCambioDeluxe extends Command
{

    protected $signature = 'actualizar_tracker_cambio_deluxe';

    protected $description = 'Actualiza todos los cambios deluxe, consumiendo el estatus de logix.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=';
        $gestiones = Gestion::where('en_proceso', true)
                                ->where('tipo_servicio', 2)
                                ->get();
        
        foreach($gestiones as $key => $gestion){
            $tracker = TrackerCambioDeluxe::where('gestion_id', $gestion->id)->first();
            $gestion_actual = Gestion::find($gestion->id);
            $servicio_logistico_recojo = ServicioLogistico::where('gestion_id', $gestion->id)
                                                    ->where('tipo', 1)
                                                    ->first();
            $servicio_logistico_entrega = ServicioLogistico::where('gestion_id', $gestion->id)
                                                    ->where('tipo', 2)
                                                    ->first();
            if($gestion_actual->estado < 6){
                $request = $client->request('GET', $url.$servicio_logistico_recojo->waybillNumber);
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];

                foreach($estados as $key => $item){
                    if(isset($item['remarks'])){
                        if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 2){
                            $tracker->update([
                                'en_camino' => true,
                                'nombre_estado' => 'En camino',
                                'estado' => 3
                            ]);
                            $gestion_actual->update([
                                'estado' => 3
                            ]);
                        }
                        if($item['remarks'] == "Product has been picked-up" && $tracker->estado == 3){
                            $tracker->update([
                                'producto_recogido' => true,
                                'nombre_estado' => 'Producto recogido',
                                'estado' => 4
                            ]);
                            $gestion_actual->update([
                                'estado' => 4
                            ]);
                        }
                        if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 4){
                            $tracker->update([
                                'producto_devuelto' => true,
                                'nombre_estado' => 'Producto devuelto',
                                'estado' => 5
                            ]);
                            $gestion_actual->update([
                                'estado' => 5
                            ]);
                        }
                    }
                }
            }else if($gestion_actual->estado > 6){
                $request = $client->request('GET', $url.$servicio_logistico_entrega->waybillNumber);
                $response = $request->getBody()->getContents();
                $str=str_replace("\r\n","",$response);
                $array_response = json_decode($str, true);
                $estados = $array_response['waybillTrackDetailList'][0]['waybillTrackingDetail'];

                foreach($estados as $key => $item){
                    if(isset($item['remarks'])){
                        if($item['remarks'] == "In transit/on its way to pick-up" && $tracker->estado == 6){
                            $tracker->update([
                                'producto_nuevo_en_camino' => true,
                                'nombre_estado' => 'En camino',
                                'estado' => 8
                            ]);
                            $gestion_actual->update([
                                'estado' => 8
                            ]);
                        }
                        if($item['remarks'] == "Product has been dropped off" && $tracker->estado == 7){
                            $tracker->update([
                                'producto_nuevo_entregado' => true,
                                'nombre_estado' => 'Producto devuelto',
                                'estado' => 9
                            ]);
                            $gestion->update([
                                'estado' => 9,
                                'en_proceso' => false,
                                'finalizado' => true
                            ]);
                        }
                    }
                }
            }
        }
    }
}