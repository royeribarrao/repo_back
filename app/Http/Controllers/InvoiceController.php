<?php

namespace App\Http\Controllers;

use App\Services\Contracts\InvoiceInterface as InvoiceService;
use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userService;

    public function __construct(
        InvoiceService $invoice
    )
    {
        $this->invoiceService = $invoice;
    }


    public function get(Request $request) {
        $invoices = $this->invoiceService->paginate();    
        return response()->json($invoices);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {

        $newInvoice = $this->invoiceService->create($request->all());
        $this->makePdf($request, $newInvoice->pdf_url);

        return response()->json([
            'state'=> 1,
            'message' => 'Proforma creada correctamente.',
            'pdf_url' => $newInvoice->pdf_url
        ]);
    }

    public function show($userId) {
        $invoices = $this->invoiceService->find($userId);
        return response()->json($invoices);
    }

    public function update($id, Request $request) {
        $invoice = $this->invoiceService->update($request->all(), $id);
        $this->makePdf($request, $invoice->pdf_url);

        return response()->json([
            'state'=> 1,
            'message' => 'Proforma actualizada correctamente.',
            'pdf_url' => $invoice->pdf_url
        ]);
    }
    
    public function destroy($id)
    {
        $res = $this->invoiceService->delete($id);
        return response()->json([
            'message' => $res['message'],

        ]);
    }


    public function makePdf(Request $request, $pdf_url) {
        $diagnosticos = $request->diagnosticos;
        $descuentos = $request->descuentos;

        $paciente = $request->paciente;
        $pacienteFechaNacim = $paciente['birthdate'];

        function getEdad($fecha_nacimiento) { 
            $tiempo = strtotime($fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
            return $edad; 
        } 
        $hoy = date("d") . "/" . date("m") . "/" . date("Y");
        

        $pdf = new Fpdf('P', 'mm', array(80,297));
        $pdf->AddPage('P',array(207,290.8));
   
        //cabecera izquierda 1
        $pdf->Cell(96.5, 15, '',0,1);
        $pdf->Image(base_path('public').'/images/marca-agua-sinFondo.png',45, 90, 130);
        $pdf->Image(base_path('public').'/images/membrete-header.jpg',10, 13, 192);
            //Información del paciente
        $pdf->SetXY(10, 36);
        $pdf->SetFont('Arial','B',8.4);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(38, 2.8, 'APELLIDOS Y NOMBRES:');

        $pdf->SetFont('Arial','',8.4);
        $nombreCorto = substr( strtoupper($paciente['nombre']) , 0, 33) . '.';
        $pdf->Cell(70, 2.8, $nombreCorto);

        $pdf->SetFont('Arial','B',8.4);
        $pdf->Cell(0.1, 2.8, 'EDAD:');


        $pdf->SetFont('Arial','',8.4);
        if (getEdad($pacienteFechaNacim) <= 9) {
            $pdf->Cell(23.6, 2.8,"0". getEdad($pacienteFechaNacim) . utf8_decode(" años"),0, 0, 'R');
        }else{
            $pdf->Cell(23.6, 2.8, getEdad($pacienteFechaNacim) . utf8_decode(" años"),0, 0, 'R');
        }

        $pdf->Cell(26.5, 2.8, '');
        $pdf->SetFont('Arial','B',8.4);

        $pdf->Cell(23, 2.8, 'DNI:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(10, 2.8, $paciente['dni'],0,0, 'R');

        $pdf->SetXY(10, 41);
        $pdf->SetFont('Arial','B',8.4);

        $pdf->Cell(10, 2.8, 'SEDE:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(148, 2.8, strtoupper($paciente['sede']));
        $pdf->SetFont('Arial','B',8.4);
        $pdf->Cell(12, 2.8, 'FECHA:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(22, 2.8, $hoy,0,0, 'R');

        // TITULOS
        $pdf->SetXY(10, 48);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFillColor(0, 74, 197);
        $pdf->SetFont('Arial','B',14);
        $pdf->Cell(191, 9.3, utf8_decode('PRESUPUESTO DE INTERVENCIÓN'),0,1,'C','B,T,R,L');
        
        
        // ***** TABLA *****
        // *** CABECERA ***
        $pdf->SetXY(10.1, 57.4);

        $pdf->SetFillColor(255,13,14);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40, 5, utf8_decode('ITEM'),0,0,'C','B,T,R,L');
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(105, 5, utf8_decode('DIAGNOSTICO'),0,0,'C','B,T,R,L');
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(46, 5, utf8_decode('PRESUPUESTO'),0,1,'C','B,T,R,L');
        
        $pdf->SetXY(10 , 65);

        //  ***** ITEMS *****
       
        $pdf->SetXY(10, 65);
        for ($x = 0; $x < count($diagnosticos) ; $x++) { 
                   
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "B", 12);
            $pdf->Cell(40, 10, utf8_decode( $x + 1 ),0,0,'C');
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "B", 12);
            $pdf->Cell(150, 10, utf8_decode(strtoupper($diagnosticos[ $x ]['description'])),0,1,'L');

        }
        
         // SUBTOTAL
        $sum = 146;
        for($d=0; $d < count($descuentos) ; $d++){
          $sum = $sum - 9;
        }
        $pdf->SetXY(10, $sum);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "", 12);
        $pdf->Cell(135, 10, utf8_decode('SUBTOTAL'),0,0,'R');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "", 12);
        $pdf->Cell(10, 10, utf8_decode(''),0,0,'C');


        $pdf->SetFillColor(255, 255, 255 );
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "", 12);
        $pdf->Cell(10, 10, utf8_decode('S/.'),0,0,'R');

        $pdf->SetFillColor(255, 255, 255 );
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "", 12);
        $pdf->Cell(31, 10, utf8_decode(number_format($request->subtotal, 2, '.', ',')),0,0,'R');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "", 12);
        $pdf->Cell(5, 10, utf8_decode(''),0,1,'C');

        // DESCUENTO - 1
      

        for ($d=0; $d < count($descuentos) ; $d++) { 
           
            $descuentoItem = $request->subtotal * (intval($descuentos[$d]['discount'])/100);

            // ESPACIO inicial
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "", 11);
            $pdf->Cell(15, 7, utf8_decode(''),0,0,'C');
            // lista de descuento - descripcion
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFillColor(251,18,18);
            $pdf->SetFont('Arial', "B", 10);
            $pdf->Cell(100, 7, utf8_decode(strtoupper($descuentos[$d]['description']) . '    '),0,0,'R');

            
            // descuento en porcentaje
            $pdf->SetFillColor(255, 0, 0 );
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Arial', "B", 11);
            $pdf->Cell(20, 7, number_format($descuentos[$d]['discount'])."%",0,0,'C','B,T,R,L');
    
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "", 12);
            $pdf->Cell(10, 7, utf8_decode(''),0,0,'C');

            
            $pdf->SetFillColor(255, 0, 0 );
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Arial', "B", 12);
            $pdf->Cell(10, 7, 'S/.',0,0,'R','B,T,R,L');

            $pdf->SetFillColor(255, 0, 0 );
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Arial', "B", 12);
            $pdf->Cell(31, 7, number_format($descuentoItem, 2, '.', ','),0,0,'R','B,T,R,L');


            $pdf->SetFillColor(250, 0, 0);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "", 12);
            $pdf->Cell(5, 7, '',0,1,'C');
    
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial', "B", 11);
            $pdf->Cell(191, 2, '',0,1,'C');
        }



         // TOTAL 
        $pdf->SetXY(10, 155);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 12);
        $pdf->Cell(135, 11.3, utf8_decode('MONTO TOTAL'),0,0,'R');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 13);
        $pdf->Cell(10, 10, utf8_decode(''),0,0,'C');


        $pdf->SetFillColor(255, 255, 255 );
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 13);
        $pdf->Cell(10, 11, utf8_decode('S/.'),0,0,'R');

        $pdf->SetFillColor(255, 255, 255 );
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 13);
        $pdf->Cell(31, 11, utf8_decode(number_format($request->total, 2, '.', ',')),0,0,'R');


        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 13);
        $pdf->Cell(5, 10, utf8_decode(''),0,1,'C');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 13);
        $pdf->Cell(5, 5, utf8_decode(''),0,1,'C');

        
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial', "B", 12);
        $pdf->Cell(191, 5, utf8_decode(''),0 ,1,'C');

        //firma
        $pdf->Image(base_path('public').'/images/texto-firma-nuevo.png',18 , 174, 173);

        $pdf->Image(base_path('public').'/images/membrete-footer.jpg',3.6 , 254, 200);
        //cierre pdf   
        $pdf->Output(base_path('public').'/presupuestos/'. $pdf_url, 'F');

        return response()->json([
            'state'=> 1,
            'message' => 'Pdf creado correctamente.',
            'url' => $pdf_url
        ]);
    
    }
}
