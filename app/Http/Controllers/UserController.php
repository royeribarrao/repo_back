<?php

namespace App\Http\Controllers;

use App\Services\Contracts\UserInterface as UserService;
use App\Services\Contracts\MaintenanceSedeInterface as SedeService;
use Illuminate\Http\Request;
use App\Models\Gestion;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userService;
    protected $sedeService;

    public function __construct(
        UserService $user,
        SedeService $sede

    )
    {
        $this->sedeService = $sede;
        $this->userService = $user;
    }


    public function get(Request $request) {
        $req = $request->all();
        $users = $this->userService->paginate(15, $req);    
        return response()->json($users);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {

        $this->userService->create($request->all());           
        return response()->json([
            'state'=> 1,
            'message' => 'Usuario creado correctamente.'
        ]);
    }
    
    public function loginCliente(Request $request){
        return response()->json([
            'state'=> 200,
            'message' => 'Login correcto',
            'data' => $request->all()
        ]);
    }

    public function show($id) {
        $user = $this->userService->find($id);
        return response()->json($user);
    }

    public function update($id, Request $request) {
        $this->userService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Usuario actualizado correctamente.'
        ]);
    }

    public function getProfiles(){
        $profiles = $this->userService->getProfiles();
        return $profiles;
    }

    public function getSedes(){
        $sedes = $this->sedeService->all();
        return $sedes;
    }

    public function logout() {
        $user = auth()->user();
        $user->tokens->each(function($token, $key) {
            $token->delete();
        });
    }

    public function getProcesoById($id){
        $procesos = Gestion::with([
                        'productoGestion.producto', 
                        'nuevosProductos.producto', 
                        'cliente', 
                        'tienda', 
                        'trackerCambioEstandar',
                        'trackerDevolucion',
                        'trackerCambioDeluxe',
                        'servicioLogistico'
                    ])
                    ->where('cliente_id', $id)
                    ->get();
        return $procesos;
    }
}
