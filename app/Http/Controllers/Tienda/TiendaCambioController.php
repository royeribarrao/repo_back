<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\TrackerCambioEstandar;

class TiendaCambioController extends Controller
{
    public function allCambios(Request $request){
        $gestiones = Gestion::with(['producto', 'cambio', 'servicio', 'cliente'])->whereIn('tipo_servicio', [1,2])->paginate(15);
        return $gestiones;
    }

    public function updateState(Request $request, $id){
        $gestion = Gestion::find($id);
        $tracker = TrackerCambioEstandar::where('gestion_id',$gestion->id)->first();
        
        $estado_gestion = $gestion->estado;
        
        switch($estado_gestion) {
            
            case(4):
            
                $gestion->update([
                    'estado' => 5
                ]);
                $tracker->update([
                    'producto_devuelto' => true,
                    'nombre_estado' => 'Producto devuelto',
                    'estado' => 5
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;

            case(5):
            
                $gestion->update([
                    'estado' => 6,
                ]);
                $tracker->update([
                    'cambio_aceptado' => true,
                    'nombre_estado' => 'Cambio Aceptado',
                    'estado' => 6
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;

             default:
                $msg = 'Something went wrong.';
        }
    }
}
