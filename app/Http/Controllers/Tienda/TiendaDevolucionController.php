<?php

namespace App\Http\Controllers\Tienda;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\TrackerDevolucion;

class TiendaDevolucionController extends Controller
{
    public function allCambios(Request $request){
        $gestiones = Gestion::with(['producto', 'cambio', 'servicio', 'cliente', 'trackerDevolucion'])->whereIn('tipo_servicio', 3)->paginate(15);
        return $gestiones;
    }

    public function updateState(Request $request, $id){
        $gestion = Gestion::find($id);
        $tracker = TrackerDevolucion::where('gestion_id',$gestion->id)->first();
        $estado_gestion = $gestion->estado;
        switch($estado_gestion) {
 
            case(2):
                 
                $gestion->update([
                    'estado' => 3
                ]);
                $tracker->update([
                    'en_camino' => true,
                    'nombre_estado' => 'En camino',
                    'estado' => 3
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
 
                break;

            case(3):
                
                $gestion->update([
                    'estado' => 4
                ]);
                $tracker->update([
                    'producto_recogido' => true,
                    'nombre_estado' => 'Producto Recogido',
                    'estado' => 4
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;
            
            case(4):
            
                $gestion->update([
                    'estado' => 5
                ]);
                $tracker->update([
                    'producto_devuelto' => true,
                    'nombre_estado' => 'Producto devuelto',
                    'estado' => 5
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;

            case(5):
            
                $gestion->update([
                    'estado' => 6,
                ]);
                $tracker->update([
                    'devolucion_aceptada' => true,
                    'nombre_estado' => 'Devolución Aceptada',
                    'estado' => 6
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;
            
            case(6):
        
                $gestion->update([
                    'estado' => 7,
                    'en_proceso' => false,
                    'finalizado' => true
                ]);
                $tracker->update([
                    'dinero_devuelto' => true,
                    'nombre_estado' => 'Dinero Devuelto',
                    'estado' => 7
                ]);

                return response()->json([
                    'state'=> 3,
                    'message' => 'Gestión actualizado correctamente.'
                ]);
    
                break;
 
            default:
                $msg = 'Something went wrong.';
        }
    }
}
