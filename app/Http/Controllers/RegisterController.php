<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    protected function validator(array $data)
    {
        $user = null;
        return User::where('email', '=', $data['email'])->get();
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        return $validator;
    }

    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return response()->json([
            'state'=> 200,
            'message' => 'Registro completado con éxito.'
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'state'=> 422,
                'message' => 'Asegurese de no estar registrado y que sus contraseñas coincidan'
            ]);
        }else{
            $user =  User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'fullname' => $request->fullname,
                'rol_id' => 2,
                'address' => $request->address,
                'distrito' => $request->distrito,
                'departamento' => $request->departamento
            ]);
            return response()->json([
                'state'=> 200,
                'message' => 'Registro completado con éxito.'
            ]);
        }
    }
}
