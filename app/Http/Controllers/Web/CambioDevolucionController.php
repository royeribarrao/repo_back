<?php

namespace App\Http\Controllers\Web;

use App\Models\Sale;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CambioDevolucionController extends Controller
{
    public function getSaleByCompra(Request $request){
        $sales = Sale::with('producto')
                    ->select(
                        'id',
                        'numero_compra',
                        'fecha_compra',
                        'sku_producto',
                        'sku_descripcion',
                        'marca',
                        'cantidad',
                        'sku_precio',
                        'descuento',
                        'precio_final',
                        'nombre_cliente',
                        'telefono',
                        'email',
                        'direccion',
                        'provincia',
                        'departamento',
                        'tipo_documento',
                        'numero_documento'
                    )
                    ->where( function ($q) use ($request){
                        if ($request->codigo_compra) {
                            $q->where('sales.numero_compra', '=', $request->codigo_compra);
                        }
                        if ($request->tienda) {
                            $q->where('sales.tienda_id', '=', $request->tienda);
                        }
                    })
                    ->get();
        return $sales;
    }
    
    public function getAllProductsByStore(Request $request){
        $products = Product::take(10)->get();
        return $products;
    }
}
