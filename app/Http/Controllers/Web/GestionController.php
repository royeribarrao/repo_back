<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\DatosDelivery;
use Carbon\Carbon;

class GestionController extends Controller
{
    public function allCambios(Request $request){
        $gestiones = Gestion::where('tipo_servicio', '=', 2)->get();
        return $gestiones;
    }

    public function allDevoluciones(Request $request){
        $gestiones = Gestion::where('tipo_servicio', '=', 1)->get();
        return $gestiones;
    }

    public function storeInvitado(Request $request)
    {
        //print_r($request->all());
        //return gettype($request->all());
        $data_cliente = json_decode($request->all()['cliente'], true);
        $info = json_decode($request->all()['info'], true);
        $info_gestion = $info['gestion'];
        $info_datos_envio = $info['datos_envio'];
        $info_nuevos_productos = $info['nuevos_productos'];
        $all_new_productos = [];

        $imagen = "";
        if($info_gestion[0]['imagen']){
            $imagen = $info_gestion[0]['imagen']['imagen']['thumbUrl'];
        }

        $new_delivery = DatosDelivery::create($data_cliente);
        
        $new_gestion = Gestion::create([
            'codigo_repo' => "",
            'fecha_solicitud' => $data_cliente['fecha_recojo'],
            'tipo_servicio' => $info_datos_envio['codigo'],
            'codigo_compra' => $info_gestion[0]['producto']['numero_compra'],
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => $new_delivery->id,
            'imagen_evidencia' => $imagen,
            'total_pago' => $info['total_pago'],
            'total_devolucion' => $info['total_devolucion']
        ]);
        
        if($new_gestion->tipo_servicio == 1 || $new_gestion->tipo_servicio === 2){
            Gestion::where('id', $new_gestion->id)->update(['codigo_repo' => "C".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }
        if($new_gestion->tipo_servicio == 3){
            Gestion::where('id', $new_gestion->id)->update(['codigo_repo' => "D".str_pad($new_gestion->id, 6, "0", STR_PAD_LEFT)]);
        }

        foreach($info_gestion as $key => $item){
            ProductoGestion::create([
                'sku_producto' => $item['producto']['sku_producto'],
                'gestion_id' => $new_gestion->id
            ]);
        }

        if(count($info_nuevos_productos) > 0 && $info_datos_envio['codigo'] !== 3){
            foreach($info_nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'sku_producto' => $item['sku_producto'],
                    'gestion_id' => $new_gestion->id
                ]);
            }
        }

        return response()->json([
            'state'=> 1,
            'data' => Gestion::with('delivery')->where('id', '=', $new_gestion->id)->first(),
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function storeLogueado(Request $request)
    {
        $info = json_decode($request->all()['info'], true);
        $info_gestion = $info['gestion'];
        $info_datos_envio = $info['datos_envio'];
        $info_nuevos_productos = $info['nuevos_productos'];
        $all_new_productos = [];
        $imagen = "";

        if($info_gestion[0]['imagen']){
            $imagen = $info_gestion[0]['imagen']['imagen']['thumbUrl'];
        }

        $new_gestion = Gestion::create([
            'fecha_solicitud' => Carbon::now()->format('Y-m-d H:i:s'),
            'tipo_servicio' => $info_datos_envio['codigo'],
            'codigo_compra' => $info_gestion[0]['producto']['numero_compra'],
            'cliente_id' => 3,
            'tienda_id' => 1,
            'datos_delivery_id' => $new_delivery->id,
            'imagen_evidencia' => $imagen,
            'total_pago_tienda' => 'pago tienda',
            'total_pago_logistico' => 'pago logistico'
        ]);

        foreach($info_gestion as $key => $item){
            ProductoGestion::create([
                'sku_producto' => $item['producto']['sku_producto'],
                'gestion_id' => $new_gestion->id
            ]);
        }

        if(count($info_nuevos_productos) > 0 && $info_datos_envio['codigo'] !== 3){
            foreach($info_nuevos_productos as $key => $item){
                NuevoProductoServicioCambio::create([
                    'sku_producto' => $item['sku_producto'],
                    'gestion_id' => $new_gestion->id
                ]);
            }
        }

        return response()->json([
            'state'=> 1,
            'data' => $new_gestion,
            'message' => 'Solicitud creada correctamente.'
        ]);
    }

    public function getByCodigo(Request $request, $codigo, $tienda_id)
    {
        $gestion = Gestion::with('trackerDevolucion')
                        ->where('codigo_repo', $codigo)
                        ->where('tienda_id', $tienda)
                        ->first();
        return $gestion;
    }
}
