<?php

namespace App\Http\Controllers;

use App\Services\Contracts\StoreInterface as StoreService;
use Illuminate\Http\Request;
use App\Models\Tienda;

class StoreController extends Controller
{
    protected $storeService;

    public function __construct(
        StoreService $store
    )
    {
        $this->storeService = $store;
    }

    public function get(Request $request) {
        $req = $request->all();
        $stores = $this->storeService->paginate(15, $req);    
        return response()->json($stores);
    }

    public function getAll(Request $request) {
        $stores = $this->storeService->all();    
        return response()->json($stores);
    }

    public function allWeb(Request $request) {
        $stores = Tienda::all();    
        return response()->json($stores);
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function store(Request $request) {

        $this->storeService->create($request->all());           
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda creada correctamente.'
        ]);
    }
    
    public function show($id) {
        $store = $this->storeService->find($id);
        return response()->json($store);
    }

    public function update($id, Request $request) {
        $this->storeService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda actualizada correctamente.'
        ]);
    }

    public function state(Request $request) {
        $this->storeService->updateState($request->id, $request->state);
        return response()->json([
            'state'=> 1,
            'message' => 'Tienda actualizado correctamente.'
        ]);
    }
}