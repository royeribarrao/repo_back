<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\DatosDelivery;
use App\Models\Tienda;
use App\Models\TrackerCambioDeluxe;
use App\Models\ServicioLogistico;
use App\Models\Carrier;

class CambioDeluxeController extends Controller
{
    public function obtenerGestionByID(Request $request, $id){
        $gestion = Gestion::find($id);
        return $gestion;
    }

    public function cambiosTodosErrorPorTienda(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto', 
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->where('tipo_servicio', 2)
                            ->where('imagen_evidencia', '!=', '')
                            ->where('estado', 1)
                            ->where('en_proceso', false)
                            ->paginate(15);
        return $gestiones_error;
    }

    public function aceptarCambioError(Request $request, $id){
        $gestion = Gestion::find($id);
        $gestion->update([
            'en_proceso' => 1
        ]);
        return $gestion;
    }

    public function denegarCambioError(Request $request){
        $gestiones_error = Gestion::with([
                                'productoGestion.producto', 
                                'nuevosProductos', 
                                'cliente', 
                                'tienda', 
                                'trackerDevolucion'
                            ])
                            ->where('tipo_servicio', 2)
                            ->where('imagen_evidencia', '!=', '')
                            ->paginate(15);
        return $gestiones_error;
    }

    public function allCambios(Request $request){
        $gestiones = Gestion::with([
                            'productoGestion.producto', 
                            'nuevosProductos.producto', 
                            'cliente', 
                            'tienda', 
                            'trackerCambioDeluxe',
                            'servicioLogistico'
                        ])
                        ->where('tipo_servicio', 2)
                        ->where('en_proceso', true)
                        ->paginate(15);
        return $gestiones;
    }

    public function updateState($id, $carrier_codigo, $waybillNumber){
        $gestion = Gestion::find($id);
        $tracker = TrackerCambioDeluxe::where('gestion_id',$gestion->id)->first();
        $carrier = Carrier::where('codigo', $carrier_codigo)->first();
        $estado_gestion = $gestion->estado;
        
        if($estado_gestion == 1){
            $gestion->update([
                'estado' => 2
            ]);
            $servicio_logistico = ServicioLogistico::create([
                'gestion_id' => $id,
                'carrier_id' => $carrier->id,
                'waybillNumber' => $waybillNumber,
                'tipo' => 1
            ]);
            $tracker->update([
                'operador_logistico_confirmado' => true,
                'nombre_estado' => 'Operador Logístico Confirmado',
                'estado' => 2
            ]);
            return response()->json([
                'state'=> true,
                'message' => 'Gestión actualizado correctamente.'
            ]);
        }else if($estado_gestion == 6){
            $gestion->update([
                'estado' => 7
            ]);
            // $servicio_logistico = ServicioLogistico::create([
            //     'gestion_id' => $id,
            //     'carrier_id' => $carrier->id,
            //     'waybillNumber' => $waybillNumber,
            //     'tipo' => 2
            // ]);
            $tracker->update([
                'operador_logistico_confirmado' => true,
                'nombre_estado' => 'Operador Logístico Confirmado',
                'estado' => 7,
            ]);
            return response()->json([
                'state'=> true,
                'message' => 'Gestión actualizado correctamente.'
            ]);
        }else if($estado_gestion != 1){
            return response()->json([
                'state'=> false,
                'message' => 'El operador logístico ya fue seleccionado.'
            ]);
        }
    }

    public function createWayBill(Request $request, $id){
        $carrier = $request->all();
        $gestion = Gestion::find($id);
        $datos_delivery = DatosDelivery::find($gestion->datos_delivery_id);
        $tienda = Tienda::find($gestion->tienda_id);
        //4 pegues para 
        $body = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => "$datos_delivery->celular",
                "CustomerAddress" => "$datos_delivery->direccion",
                "CustomerName" => "$datos_delivery->nombres",
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => "$datos_delivery->latitud,$datos_delivery->longitud",
                "DeliveryDate" => "$gestion->fecha_recojo",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeAddress" => "$tienda->address",
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeEmail" => "$tienda->email",
                "consigneeGeoLocation" =>"$tienda->latitud,$tienda->longitud",
                "ConsigneeName" => "$tienda->business_name",
                "ConsigneePhone" => "$tienda->phone",
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$gestion->codigo_repo",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                            "barCode" => "",
                            "packageCount" => 1,
                            "length" => 1.0,
                            "width" => 1.0,
                            "height" => 1.0,
                            "weight" => 1.0,
                            "itemCount" => 1,
                            "chargedWeight" => 1.0,
                            "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        $body2 = [
            "waybillRequestData" =>  [
                "FromOU" => "REPO",
                "WaybillNumber" => "",
                "CustomerCountry" => "PE",
                "CustomerState" => "LIMA",
                "CustomerCity" => "LIMA",
                "CustomerPhone" => "$datos_delivery->celular",
                "CustomerAddress" => "$datos_delivery->direccion",
                "CustomerName" => "$datos_delivery->nombres",
                "CustomerPincode" => "02002",
                "consignorGeoLocation" => "$tienda->latitud,$tienda->longitud",
                "DeliveryDate" => "$gestion->fecha_solicitud",
                "CustomerCode" => "1234",
                "ConsigneeCode" => "00000",
                "ConsigneeAddress" => "$tienda->address",
                "ConsigneeCountry" => "PE",
                "ConsigneeState" => "LIMA",
                "ConsigneeCity" => "LIMA",
                "ConsigneePincode" => "020012",
                "ConsigneeEmail" => "$tienda->email",
                "consigneeGeoLocation" =>"$datos_delivery->latitud,$datos_delivery->longitud",
                "ConsigneeName" => "$tienda->business_name",
                "ConsigneePhone" => "$tienda->phone",
                "ClientCode" => "1234",
                "NumberOfPackages" => 1,
                "ActualWeight" => 1.0,
                "ChargedWeight" => 1.0,
                "CargoValue" => 1.0,
                "ReferenceNumber" => "$gestion->codigo_repo",
                "InvoiceNumber" => "",
                "PaymentMode" => "TBB",
                "ServiceCode" => "EXPRESS",
                "reverseLogisticActivity" => "",
                "reverseLogisticRefundAmount" => "",
                "WeightUnitType" => "KILOGRAM",
                "Description" => "",
                "COD" => 0,
                "CODPaymentMode" => "",
                "DutyPaidBy" => "",
                "WaybillPrintDesign" => "",
                "StickerPrintDesign" => "",
                "skipCityStateValidation" => "",
                "packageDetails" => [
                    "packageJsonString" => [
                            "barCode" => "",
                            "packageCount" => 1,
                            "length" => 1.0,
                            "width" => 1.0,
                            "height" => 1.0,
                            "weight" => 1.0,
                            "itemCount" => 1,
                            "chargedWeight" => 1.0,
                            "selectedPackageTypeCode" => "BOX"
                    ]
                ]
            ]
        ];
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/CreateWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $headers = array( 
            'AccessKey' => 'logixerp',
            'Content-Type' => 'application/json'
            );
        if($gestion->estado == 1){
            $create_waybill = $client->request('POST', $url, [
                'headers' => ['AccessKey' => 'logixerp', 'Content-Type' => 'application/json'],
                'json' => $body
            ]);
        }else if($gestion->estado == 6){
            $create_waybill = $client->request('POST', $url, [
                'headers' => ['AccessKey' => 'logixerp', 'Content-Type' => 'application/json'],
                'json' => $body2
            ]);
        }else{
            return response()->json([
                'state'=> false,
                'message' => 'Espere el estado correcto para elegir el operador logistico.'
            ]);
        }
        
        return $create_waybill;
    }

    public function generateCarrierWayBill(Request $request, $id, $carrier_codigo){
        $client = new \GuzzleHttp\Client();
        $gestion = Gestion::find($id);
        $carrier_cod = $carrier_codigo;
        $carrierProduct = "BOX";
        $carrier_cod = "OLVA";
        if($carrier_codigo == 'CARGUI'){
            if($gestion->tipo_servicio == 1 || $gestion->tipo_servicio == 3){
                $carrierProduct = 3;
            }else if($gestion->tipo_servicio == 2){
                $carrierProduct = 2;
            }
        }
        $url = 'https://api.logixplatform.com/webservice/v2/GenerateCarrierWaybill?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559';
        $generate_carrier = $client->request('POST', $url, 
        [
            'multipart' => [
                [
                    'name' => 'waybillNumber',
                    'contents' => $request->data['waybillNumber']
                ],
                [
                    'name' => 'carrierCode',
                    'contents' => $carrier_cod
                ],
                [
                    'name' => 'carrierProduct',
                    'contents' => $carrierProduct
                ],
            ],
        ]);
        return $this->updateState($id, $carrier_codigo, $request->data['waybillNumber']);
    }

    public function obtenerEstado(){
        $client   = new \GuzzleHttp\Client();
        $url = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=JOBERGFTL285';
        $url_nirex = 'https://api.logixplatform.com/webservice/v2/MultipleWaybillTracking?secureKey=425B4F4FF84046AE8BEF2F3F11EEB559&waybillNumber=JOBERGEXPRESS235';
        $request = $client->request('GET', $url);
        return $request;
    }

    public function actualizarTracker(Request $request, $id){
        $gestion = Gestion::with('tienda')->find($id);
        $estados = [];
        if(!$gestion->finalizado){
            if($gestion->tipo_servicio == 1){
                $tracker = TrackerCambioDeluxe::where('gestion_id', $id)->first();
                
                foreach($request->waybillTrackDetailList[0]['waybillTrackingDetail'] as $key => $item){
                    if(isset($item['remarks'])){
                        if($item['remarks'] == "Order has been assigned to an Olva driver"){
                            $tracker->update([
                                'operador_logistico_confirmado' => true
                            ]);
                        }
                        if($item['remarks'] == "In transit/on its way to pick-up"){
                            $tracker->update([
                                'en_camino' => true
                            ]);
                        }
                        if($item['remarks'] == "Product has been picked-up"){
                            $tracker->update([
                                'producto_recogido' => true
                            ]);
                        }
                        if($item['remarks'] == "Product has been dropped off"){
                            $tracker->update([
                                'producto_devuelto' => true
                            ]);
                        }
                    }
                    if(isset($item['remarks'])){
                        $estados[$key] = $item['remarks'];
                    }
                }
            }else if($gestion->tipo_servicio == 2){
                $tracker = TrackerCambioDeluxe::where('gestion_id', $id)->first();
                foreach($request->waybillTrackDetailList[0]['waybillTrackingDetail'] as $key => $item){
                    if($item['remarks'] == "Order has been assigned to an Olva driver"){
                        $tracker->update([
                            'operador_logistico_confirmado' => true
                        ]);
                    }
                    if($item['remarks'] == "In transit/on its way to pick-up"){
                        $tracker->update([
                            'en_camino' => true
                        ]);
                    }
                    if($item['remarks'] == "Product has been picked-up"){
                        $tracker->update([
                            'producto_recogido' => true
                        ]);
                    }
                    if($item['remarks'] == "Product has been dropped off"){
                        $tracker->update([
                            'producto_devuelto' => true
                        ]);
                    }
                    if(isset($item['remarks'])){
                        $estados[$key] = $item['remarks'];
                    }
                }
            }
        }
        return $estados;
    }

    public function getById($id){
        $gestion = Gestion::find($id);
        return $gestion;
    }
}