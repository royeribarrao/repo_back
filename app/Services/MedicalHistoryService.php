<?php namespace App\Services;

use App\Services\Contracts\MedicalHistoryInterface;
use App\Models\MedicalHistory as MedicalHistoryModel;

class MedicalHistoryService implements MedicalHistoryInterface {
  
  protected $medicalHistory;
 
  
  public function __construct(
    MedicalHistoryModel $medicalHistory)
  {
    $this->medicalHistory = $medicalHistory;
  }
  
  public function all($columns = array('*'), $relation = [])
  {
    return $this->medicalHistory->where('patient_id', $relation['patient_id'])
      ->orderBy('id', 'desc')
      ->get($columns);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    return $this->medicalHistory->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {

    $newMedicalHistory = $this->medicalHistory->create($data);

    if ($newMedicalHistory->active == 1) {
      $this->medicalHistory->where('id', '!=', $newMedicalHistory->id)
        ->update(['active' => 2 ]);
    }

    return $newMedicalHistory;
  }
  
  public function update(array $data, $id)
  {
    $medicalHistoryGot = $this->medicalHistory->find($id);
    $medicalHistoryGot->fill($data);
    $medicalHistoryGot->save();

    if ($medicalHistoryGot->active == 1) {
      $this->medicalHistory->where('id', '!=', $id)
        ->update(['active' => 2 ]);
    }
    
    return $medicalHistoryGot;
  }
  
  public function delete($id)
  {
    $this->medicalHistory->destroy($id);
    return ['message' => 'Historial medico eliminada'];
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->medicalHistory
      ->with('diagnostics', 'discounts')
      ->where('patient_id', $id)
      ->orderBy('id', 'desc')
      ->get();
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->medicalHistory->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Implement search() method.
  }

  public function getHistories($patientId) 
  {
    return $this->medicalHistory
      ->where('medical_histories.patient_id', $patientId)
      ->orderBy('id', 'desc')
      ->get();
  }
  
}