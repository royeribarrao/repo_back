<?php namespace App\Services;

use App\Services\Contracts\ProductInterface as ProductInterface;
use App\Models\Product as ProductModel;

class ProductService implements ProductInterface {
  
    protected $product;
    
    public function __construct(
        ProductModel $productModel
        )
    {
        $this->product = $productModel;
    }
    
    public function all($columns = array('*'), $relation = [])
    {
        return $this->product->get($columns);
    }
    
    public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
    {
        return $this->product->where(function ($q) use ($columns){
            if (isset($columns['fullname']) && $columns['fullname'] !== '') {
            $q->where('fullname','like', '%'.$columns['fullname'].'%');
            }
            if (isset($columns['dni']) && $columns['dni'] !== '') {
            $q->where('dni', $columns['dni']);
            }
            if (isset($columns['sede_id']) && $columns['sede_id'] !== '') {
            $q->where('sede_id', $columns['sede_id']);
            }
        })
        ->orderBy('id', $order_type)->paginate($perPage);
    }
    
    public function create(array $data)
    {
        $newProduct = $this->product->create($data);
        return $newProduct;
    }
    
    public function update(array $data, $id)
    {
        $productGot = $this->product->find($id);
        $productGot->update($data);
        
        return $productGot;
    }
    
    public function delete($id)
    {
        return $this->product->destroy($id);
    }
    
    public function find($id, $columns = array('*'))
    {
        return $this->product->find($id, $columns);
    }
    
    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->product->where($field, '=', $value)->first($columns);
    }
    
    public function search($searching, $columns = array('*'))
    {
        // TODO: Impselement search() method.
    }  
}