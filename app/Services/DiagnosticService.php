<?php namespace App\Services;

use App\Services\Contracts\DiagnosticInterface as DiagnosticInterface;
use App\Models\Diagnostic as DiagnosticModel;

class DiagnosticService implements DiagnosticInterface {
  
  protected $diagnostic;
  
  public function __construct(DiagnosticModel $diagnosticModel)
  {
    $this->diagnostic = $diagnosticModel;
  }
    
  public function all($columns = array('*'), $relations = [])
  {
    return $this->diagnostic->get(['id', 'name']);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    return $this->diagnostic->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {
    $newDiagnostic = $this->diagnostic->create($data);
    return $newDiagnostic;
  }
  
  public function update(array $data, $id)
  {
    $diagnosticGot = $this->diagnostic->find($id);
    $diagnosticGot->update($data);
    return $diagnosticGot;
  }
  
  public function delete($id)
  {
    return $this->diagnostic->destroy($id);
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->diagnostic->find($id, $columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->diagnostic->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Implement search() method.
  }
  
  public function updateState($id, $state)
  {
    $diagnostic = $this->diagnostic->find($id);
    $diagnostic->state = !$state;
    $diagnostic->save();
    return $diagnostic;
  }
}