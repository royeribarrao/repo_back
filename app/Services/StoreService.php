<?php namespace App\Services;

use App\Services\Contracts\StoreInterface as StoreInterface;
use App\Models\Tienda as StoreModel;

class StoreService implements StoreInterface {
  
    protected $store;
    
    public function __construct(
        StoreModel $storeModel
        )
    {
        $this->store = $storeModel;
    }
    
    public function all($columns = array('*'), $relation = [])
    {
        return $this->store->get($columns);
    }
  
    public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
    {
        return $this->store->where(function ($q) use ($columns){
            if (isset($columns['name']) && $columns['name'] !== '') {
            $q->where('business_name','like', '%'.$columns['name'].'%');
            }
            if (isset($columns['ruc']) && $columns['ruc'] !== '') {
            $q->where('ruc', $columns['ruc']);
            }
        })
        ->orderBy('id', $order_type)->paginate($perPage);
    }
    
    public function create(array $data)
    {
        $newStore = $this->store->create($data);
        return $newStore;
    }
    
    public function update(array $data, $id)
    {
        $storeGot = $this->store->find($id);
        $storeGot->update($data);
        
        return $storeGot;
    }
    
    public function delete($id)
    {
        return $this->store->destroy($id);
    }
    
    public function find($id, $columns = array('*'))
    {
        return $this->store->find($id, $columns);
    }
    
    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->store->where($field, '=', $value)->first($columns);
    }
    
    public function search($searching, $columns = array('*'))
    {
        // TODO: Impselement search() method.
    }

    public function updateState($id, $state)
    {
        $storeeGot = $this->store->find($id);
        $storeeGot->state = !$state;
        $storeeGot->save();
        return $storeeGot;
    }

    public function formatFiles($files, $id, $type) {
        return array_map( 
          function($file) use ($id, $type) { 
            if(file_exists($file)) {
                File::delete($file);
            }
            $newFileName = uniqid().'.'.$file->extension();    
            \Storage::disk('public')->put("tiendas/logos".$newFileName,  \File::get($file));
            return $newFileName;
          },
          $files
        );
    }
}