<?php
use Illuminate\Support\Facades\Route;


$router->post('registro-usuario', 'RegisterController@register');
$router->post('login-cliente', 'UserController@loginCliente');

$router->get('tiendas', 'StoreController@allWeb');
$router->get('cambios-devoluciones', 'Web\CambioDevolucionController@getSaleByCompra');
$router->get('productos', 'Web\CambioDevolucionController@getAllProductsByStore');
$router->post('gestion', 'Web\GestionController@storeInvitado');
$router->get('gestion/{codigo}/{tienda_id}', 'Web\GestionController@getByCodigo');

$router->get('tipos-servicio', 'TipoServicioController@get');

$router->post('devoluciones/{id}', 'DevolucionController@createWayBill');
$router->post('devoluciones/generate-carrier-way-bill/{id}/{carrier_codigo}', 'DevolucionController@generateCarrierWayBill');
$router->post('devoluciones-tienda/{id}', 'Tienda\TiendaDevolucionController@updateState');

$router->post('cambios/{id}', 'CambioController@createWayBill');
$router->post('cambios/generate-carrier-way-bill/{id}/{carrier_codigo}', 'CambioController@generateCarrierWayBill');


$router->post('cambios-second/{id}', 'CambioController@createWayBill2');
$router->post('cambios-tienda/{id}', 'Tienda\TiendaCambioController@updateState');

$router->get('cambios/error', 'CambioController@cambiosTodosErrorPorTienda');
$router->post('cambios/error/aceptar/{id}', 'CambioController@aceptarCambioError');
$router->post('cambios/error/denegar/{id}', 'CambioController@denegarCambioError');

$router->get('operadores-logisticos/{id}', 'OperadorLogisticoController@show');
$router->get('procesos/usuario/{id}', 'UserController@getProcesoById');

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {

    $router->get('users/sedes', 'UserController@getSedes');
    $router->get('users/profiles', 'UserController@getProfiles');
    $router->get('users/logout', 'UserController@logout');
    $router->get('users', 'UserController@get');
    $router->get('users/{id}', 'UserController@show');
    $router->post('users', 'UserController@store');
    $router->post('users/{id}', 'UserController@update');
    //Diagnostics
    //maintenaces
    $router->get('maintenances/sedes', 'MaintenanceSedeController@get');
    $router->get('maintenances/sedes/simple', 'MaintenanceSedeController@getAll');
    $router->get('maintenances/sedes/{id}', 'MaintenanceSedeController@show');
    $router->post('maintenances/sedes', 'MaintenanceSedeController@store');
    $router->post('maintenances/sedes/state', 'MaintenanceSedeController@state');
    $router->post('maintenances/sedes/{id}', 'MaintenanceSedeController@update');


    $router->get('maintenances/services', 'MaintenanceSerController@get');
    $router->get('maintenances/services/simple', 'MaintenanceSerController@getAll');
    $router->get('maintenances/services/{id}', 'MaintenanceSerController@show');
    $router->post('maintenances/services', 'MaintenanceSerController@store');
    $router->post('maintenances/services/state', 'MaintenanceSerController@state');
    $router->post('maintenances/services/{id}', 'MaintenanceSerController@update');


    $router->get('stores', 'StoreController@get');
    $router->get('stores/simple', 'StoreController@getAll');
    $router->get('stores/{id}', 'StoreController@show');
    $router->post('stores', 'StoreController@store');
    $router->post('stores/state', 'StoreController@state');
    $router->post('stores/{id}', 'StoreController@update');

    $router->get('products', 'ProductController@get');
    $router->get('products/{id}', 'ProductController@show');
    $router->post('products', 'ProductController@store');
    $router->post('products/state', 'ProductController@state');
    $router->post('products/upload-csv', 'ProductController@uploadCsv');
    $router->post('products/{id}', 'ProductController@update');

    $router->get('sales', 'SaleController@get');
    $router->get('sales/{id}', 'SaleController@show');
    $router->post('sales/state', 'SaleController@state');
    $router->post('sales/upload-csv', 'SaleController@uploadCsv');

    $router->get('devoluciones', 'DevolucionController@allDevoluciones');
    $router->get('devoluciones/{id}', 'DevolucionController@show');
    
    $router->get('cambios', 'CambioController@allCambios');
    $router->get('cambios/{id}', 'CambioController@show');

    $router->get('cambios-deluxe', 'CambioDeluxeController@allCambios');
    $router->get('cambios-deluxe/error', 'CambioDeluxeController@cambiosTodosErrorPorTienda');
    $router->get('cambio-deluxe/{id}', 'CambioDeluxeController@getById');
    
    $router->post('gestion/cambios/actualizar/{id}', 'CambioController@updateState');
    $router->get('gestion/{id}', 'CambioController@obtenerGestionByID');

    // $router->get('gestion/cambios', 'Web\GestionController@allCambios');
    // $router->get('gestion/devoluciones', 'Web\GestionController@allDevoluciones');
    $router->get('authinfo', 'UserController@getAuthenticated');
});