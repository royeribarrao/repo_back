<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TipoServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tipo_servicio')->insert([
            'codigo' => 1,
            'nombre' => 'Estandar',
            'nombre_tarifa_base' => 'Costo Base',
            'precio_base' => 9.50
        ]);

        \DB::table('tipo_servicio')->insert([
            'codigo' => 2,
            'nombre' => 'Deluxe',
            'nombre_tarifa_base' => 'Costo Base',
            'precio_base' => 11.50
        ]);

        \DB::table('tipo_servicio')->insert([
            'codigo' => 3,
            'nombre' => 'Devolución',
            'nombre_tarifa_base' => 'Costo Base',
            'precio_base' => 5.50
        ]);
    }
}
