<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SituationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('situations')->insert([
            [
                'name' => 'Pre-consulta',
            ], 
            [
                'name' => 'Consulta Nueva',
            ],
            [
                'name' => 'Tratamiento',
            ]
        ]);
    }
}
