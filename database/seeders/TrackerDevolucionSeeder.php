<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TrackerDevolucionSeeder extends Seeder
{
    public function run()
    {
        \DB::table('tracker_devolucion')->insert([
            'gestion_id' => 4,
            'pedido_recibido' => true,
            'operador_logistico_confirmado' => false,
            'en_camino' => false,
            'producto_recogido' => false,
            'producto_devuelto' => false,
            'devolucion_aceptada' => false,
            'dinero_devuelto' => false,
            'nombre_estado' => 'Pedido recibido',
            'estado' => 1
        ]);
    }
}