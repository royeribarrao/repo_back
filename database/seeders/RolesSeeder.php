<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            [
                'name' => 'User',
                'order' => '1',
            ], [
                'name' => 'Admin',
                'order' => '2',
            ],[
                'name' => 'Paciente',
                'order' => '3',
            ],[
                'name' => 'Admin',
                'order' => '4',
            ] 
        ]);
    }
}
