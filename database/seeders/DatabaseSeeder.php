<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(SedeSeeder::class);
        $this->call(RolesSeeder::class);
        //$this->call(PatientSeeder::class);
        $this->call(ProfileSeeder::class);
        //$this->call(DiagnosticSeeder::class);
        //$this->call(SimpleDiagnosticSeeder::class);
        //$this->call(AppointmentCodesSeeder::class);
        //$this->call(SituationSeeder::class);
        $this->call(StoresTableSeeder::class);
        $this->call(PrecioTipoServicioSeeder::class);
        $this->call(TipoServicioSeeder::class);
        $this->call(GesionTableSeeder::class);
        $this->call(NuevoProductoServicioCambioSeeder::class);
        $this->call(ProductoGestionSeeder::class);
        $this->call(TrackerDevolucionSeeder::class);
        $this->call(CarrierSeeder::class);
        $this->call(CarrierTarifasSeeder::class);
        $this->call(TrackerCambioDeluxeSeeder::class);
        $this->call(TrackerCambioEstandarSeeder::class);
        $this->call(DatosDeliverySeeder::class);
    }
}
