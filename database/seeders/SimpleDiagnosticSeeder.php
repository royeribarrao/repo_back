<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SimpleDiagnosticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        \DB::table('simple_diagnostics')->insert([
            [
                'name' => 'COLUMNA',
            ], 
            [
                'name' => 'CADERA',
            ],
            [
                'name' => 'RODILLA',
            ],
            [
                'name' => 'OTROS',
            ]
        ]);
    }
}
