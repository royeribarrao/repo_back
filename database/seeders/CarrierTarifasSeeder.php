<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CarrierTarifasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 3,
            'tipo_servicio_id' => 2,
            'tipo_vehiculo' => 'moto',
            'precio' => 16.00,
            'hora_maxima_pedido' => 'L-V 9:00- 13:00',
            'hora_recojo' => 'L-V 9:00- 21:00', 
            'hora_entrega' => 'Dentro de 6 horas adicionales'
        ]);

        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 2,
            'tipo_servicio_id' => 3,
            'tipo_vehiculo' => 'moto',
            'precio' => 7.00,
            'hora_maxima_pedido' => 'L-V 13:00- 23:00',
            'hora_recojo' => 'L-V 8:00- 12:00', 
            'hora_entrega' => 'L-V 8:00- 12:00'
        ]);

        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 2,
            'tipo_servicio_id' => 1,
            'tipo_vehiculo' => 'moto',
            'precio' => 7.00,
            'hora_maxima_pedido' => 'L-V 13:00- 23:00',
            'hora_recojo' => 'L-V 8:00- 12:00', 
            'hora_entrega' => 'L-V 8:00- 12:00'
        ]);

        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 2,
            'tipo_servicio_id' => 2,
            'tipo_vehiculo' => 'moto',
            'precio' => 14.00,
            'hora_maxima_pedido' => 'L-V 7:00- 13:00',
            'hora_recojo' => 'L-V 13:00- 18:00', 
            'hora_entrega' => 'L-V 13:00- 18:00'
        ]);

        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 1,
            'tipo_servicio_id' => 3,
            'tipo_vehiculo' => 'moto',
            'precio' => 6.80,
            'hora_maxima_pedido' => 'L-V 7:00- 23:00',
            'hora_recojo' => 'L-V 9:00- 13:00', 
            'hora_entrega' => 'L-V 10:00- 19:00'
        ]);

        \DB::table('carrier_tarifas')->insert([
            'carrier_id' => 1,
            'tipo_servicio_id' => 1,
            'tipo_vehiculo' => 'moto',
            'precio' => 13.60,
            'hora_maxima_pedido' => 'L-V 7:00- 23:00',
            'hora_recojo' => 'L-V 9:00- 13:00', 
            'hora_entrega' => 'L-V 10:00- 19:00'
        ]);
    }
}
