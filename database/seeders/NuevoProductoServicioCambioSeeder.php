<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NuevoProductoServicioCambioSeeder extends Seeder
{
    public function run()
    {
        \DB::table('nuevos_productos_servicio_cambio')->insert([
            'sku_producto' => '20200023044',
            'gestion_id' => 1
        ]);

        \DB::table('nuevos_productos_servicio_cambio')->insert([
            'sku_producto' => '20200043230',
            'gestion_id' => 1
        ]);
    }
}
