<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('maintenance_services')->insert([
            [
                'name' => 'Alivio del dolor',
                'alias' => 'Alivio',
                'order' => '1',
                'phone' => '934202220',
                'state' => '1',
            ], [
                'name' => 'Medilaser perú',
                'alias' => 'Medilaser',
                'order' => '2',
                'phone' => '998608060',
                'state' => '1',
            ],[
                'name' => 'Artrosis perú',
                'alias' => 'Artrosis',
                'order' => '3',
                'phone' => '998808080',
                'state' => '1'
            ],[
                'name' => 'Osteoporosis perú',
                'alias' => 'Osteoporosis',
                'order' => '4',
                'phone' => '998558449',
                'state' => '1'
            ],[
                'name' => 'Varices perú',
                'alias' => 'Varices',
                'order' => '4',
                'phone' => '998558449',
                'state' => '1'
            ] 
        ]);
    }
}
