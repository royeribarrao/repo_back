<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('users')->insert([
        'dni' => '70019408',
        'name' => 'ROYER',
        'fullname' => 'ROYER IBARRA',
        'father_lastname' => 'ibarra',
        'mother_lastname' => 'orejon',
        'email' => 'admin@admin.com',
        'password' => app('hash')->make('secret'),
        'rol_id' => '1',
        'state' => '1',
        'phone' => '939784580',
      ]);

      \DB::table('users')->insert([
        'dni' => '70070070',
        'name' => 'TIENDA',
        'fullname' => 'TIENDA FALABELLA',
        'father_lastname' => 'falabella',
        'mother_lastname' => 'falabella',
        'email' => 'tienda@tienda.com',
        'password' => app('hash')->make('secret'),
        'rol_id' => '2',
        'state' => '1',
        'phone' => '900900900'
      ]);

      \DB::table('users')->insert([
        'dni' => '99999999',
        'name' => 'Marcos',
        'fullname' => 'Marcos Enciso',
        'father_lastname' => 'Enciso',
        'mother_lastname' => 'Gomez',
        'email' => 'marcos25@gmail.com',
        'password' => app('hash')->make('secret'),
        'rol_id' => '3',
        'state' => '1',
        'phone' => '999999999'
      ]);

      \DB::table('users')->insert([
        'dni' => '10010010',
        'name' => 'nathaly',
        'fullname' => 'Nathaly Quispe',
        'father_lastname' => 'quispe',
        'mother_lastname' => 'clemente',
        'email' => 'nathquispe@gmail.com',
        'password' => app('hash')->make('secret'),
        'rol_id' => '3',
        'state' => '1',
        'phone' => '947623599'
      ]);

      \DB::table('users')->insert([
        'dni' => '20020020',
        'name' => 'usuario devolucion',
        'fullname' => 'devolucion devolucion',
        'father_lastname' => 'devolucion',
        'mother_lastname' => 'devolucion',
        'email' => 'devolucion@devolucion.com',
        'password' => app('hash')->make('secret'),
        'rol_id' => '3',
        'state' => '1',
        'phone' => '900900900'
      ]);

      \DB::table('users')->insert([
        'dni' => '20601050456',
        'name' => 'usuario inmaculada',
        'fullname' => 'inmaculada',
        'father_lastname' => 'inmaculada',
        'mother_lastname' => 'inmaculada',
        'email' => 'info@inmaculada.shop',
        'password' => app('hash')->make('inmaculada'),
        'rol_id' => '2',
        'state' => '1',
        'phone' => '993652001'
      ]);

      \DB::table('users')->insert([
        'dni' => '20608476653',
        'name' => 'Brunella',
        'fullname' => 'Brunella',
        'father_lastname' => 'Brunella',
        'mother_lastname' => 'Brunella',
        'email' => 'andrea95pal@gmail.com',
        'password' => app('hash')->make('brunella'),
        'rol_id' => '2',
        'state' => '1',
        'phone' => '989347366'
      ]);

      \DB::table('users')->insert([
        'dni' => '20603460309',
        'name' => 'gea',
        'fullname' => 'gea',
        'father_lastname' => 'gea',
        'mother_lastname' => 'gea',
        'email' => 'hola@geacorporation.com',
        'password' => app('hash')->make('holagea'),
        'rol_id' => '2',
        'state' => '1',
        'phone' => '941441237'
      ]);
    }
}
