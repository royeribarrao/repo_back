<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServicioLogisticoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('servicio_logistico')->insert([
            'gestion_id' => 1,
            'carrier_id' => 1,
            'waybillNumber' => 'JOBERGFTL285',
            'tipo' => 1
        ]);

        \DB::table('servicio_logistico')->insert([
            'gestion_id' => 1,
            'carrier_id' => 1,
            'waybillNumber' => 'JOBERGFTL285',
            'tipo' => 2
        ]);

        \DB::table('servicio_logistico')->insert([
            'gestion_id' => 4,
            'carrier_id' => 1,
            'waybillNumber' => 'JOBERGFTL285',
            'tipo' => ''
        ]);
    }
}
