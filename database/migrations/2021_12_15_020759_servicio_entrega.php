<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicioEntrega extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_entrega', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_logistico_id')->nullable();
            $table->boolean('producto_nuevo_en_camino')->nullable();
            $table->boolean('producto_nuevo_en_entregado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_entrega');
    }
}
