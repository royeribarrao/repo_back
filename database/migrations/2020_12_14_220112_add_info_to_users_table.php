<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('father_lastname', 100);
            $table->string('mother_lastname', 100);
            $table->string('address', 100);
            $table->string('email', 80);
            $table->string('fullname', 100);
            $table->string('phone', 10)->nullable();
            $table->string('distrito', 100);
            $table->string('departamento', 100);
            $table->string('password', 150);
            $table->string('dni', 20)->nullable();
            $table->smallInteger('state')->default(1)->nullable();
            $table->integer('rol_id')->nullable();
            $table->integer('place_id')->nullable();
            $table->integer('updated_user')->nullable();
            $table->integer('created_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            Schema::dropIfExists('users');
        });
    }
}
