<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NuevosProductosServicioCambio extends Migration
{
    public function up()
    {
        Schema::create('nuevos_productos_servicio_cambio', function (Blueprint $table) {
            $table->id();
            $table->string('sku_producto');
            $table->string('gestion_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nuevos_productos_servicio_cambio');
    }
}