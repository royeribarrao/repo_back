<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductosGestion extends Migration
{
    public function up()
    {
        Schema::create('productos_gestion', function (Blueprint $table) {
            $table->id();
            $table->string('sku_producto');
            $table->string('gestion_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('productos_gestion');
    }
}