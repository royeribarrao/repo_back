<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tiendas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiendas', function (Blueprint $table) {
            $table->id();
            $table->string('business_name');
            $table->string('description');
            $table->boolean('state');
            $table->string('ruc');
            $table->string('logo');
            $table->string('departamento');
            $table->string('provincia');
            $table->string('distrito');
            $table->string('address');
            $table->string('codigo_postal');
            $table->string('email');
            $table->string('phone');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('contacto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiendas');
    }
}
