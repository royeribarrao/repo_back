<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Gestiones extends Migration
{
    public function up()
    {
        Schema::create('gestiones', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->string('codigo_compra')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->string('fecha_recojo')->nullable();
            $table->integer('tipo_servicio')->nullable();
            $table->integer('datos_delivery_id')->nullable();
            $table->string('total_pago')->nullable();
            $table->string('total_devolucion')->nullable();
            $table->string('codigo_repo')->nullable();
            $table->longText('imagen_evidencia')->nullable();
            $table->integer('estado')->nullable();
            $table->boolean('en_proceso')->default(false);
            $table->boolean('finalizado')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gestiones');
    }
}
