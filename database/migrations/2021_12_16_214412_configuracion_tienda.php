<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConfiguracionTienda extends Migration
{
    public function up()
    {
        Schema::create('configuracion_tienda', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id')->nullable();
            $table->boolean('devolucion')->nullable();
            $table->boolean('estandar')->nullable();
            $table->boolean('deluxe')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configuracion_tienda');
    }
}