<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicioRecojo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_recojo', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_logistico_id')->nullable();
            $table->boolean('en_camino')->nullable();
            $table->boolean('producto_recogido')->nullable();
            $table->boolean('producto_devuelto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_recojo');
    }
}
