<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaintenancesedesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_sedes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('color', 40);
            $table->integer('order')->nullable();
            $table->smallInteger('state')->default(1)->nullable();
            $table->integer('updated_user')->nullable();
            $table->integer('created_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
 
     public function down()
    {
        Schema::table('maintenance_sedes', function (Blueprint $table) {
            Schema::dropIfExists('maintenance_sedes');
        });
    }
}
