<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ventas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->integer('tienda_id');
            $table->string('numero_compra');
            $table->string('fecha_compra');
            $table->string('sku_producto');
            $table->string('sku_descripcion');
            $table->string('marca');
            $table->string('cantidad');
            $table->float('sku_precio');
            $table->float('precio_final');
            $table->float('precio');
            $table->string('nombre_cliente');
            $table->string('telefono');
            $table->string('email');
            $table->string('direccion');
            $table->string('distrito');
            $table->string('provincia');
            $table->string('departamento');
            $table->string('tipo_documento');
            $table->string('numero_documento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
