<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku_producto');
            $table->string('sku_description');
            $table->string('marca');
            $table->string('talla');
            $table->string('imagen');
            $table->string('stock');
            $table->string('sku_precio');
            $table->string('descuento');
            $table->string('precio_final');
            $table->string('dimensiones');
            $table->string('categoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
